// @flow

import React from 'react';
import ReactDOM from 'react-dom';

import Root from './components/Root/Root';

import './styles/styles.scss';

type Props = {};

class App extends React.Component<Props> {
  render() {
    return <Root />;
  }
}

const appDiv = document.getElementById('app');
if (appDiv) {
  ReactDOM.render(<App />, appDiv);
}
