import { shallow } from 'enzyme';
import React from 'react';

import Root from '../Root';

describe('Root', () => {
  test('should render correctly', () => {
    expect(shallow(<Root />)).toMatchSnapshot();
  });
});
