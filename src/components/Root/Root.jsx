// @flow

import React from 'react';

type Props = {};

export default class Hello extends React.Component<Props> {
  render() {
    return (
      <div>
        <p>Hello World</p>
      </div>
    );
  }
}
