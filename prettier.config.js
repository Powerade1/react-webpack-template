module.exports = {
    printWidth: 80,
    singleQuote: true,
    trailingComma: 'none',
    bracketSpacing: true,
    jsxBracketSameLine: false,
    tabWidth: 2,
    arrowParens: 'avoid',
    semi: true,
    parser: 'flow'
};